# This configuration file will transform all docker container events that is produced by docker swarm when a new container is created, started, stopped, destroyed etc (https://docs.docker.com/v17.09/edge/engine/reference/commandline/events/#object-types) into a format that is common for all of our fluentd configurations. We want them in the same format so we later can filter by a certain property using Kibana and see a complete history of a container lifecycle (container started, container application logs, container destroyed etc). 

<filter docker-container-event.**>
  type flatten_hash
  separator .
</filter>

<filter docker-container-event.**>
  @type record_transformer
  <record>
    event.origin "docker"
    service.name ${record["Actor.Attributes.logging.params.service.name"]}
    git.repository.namespace ${record["Actor.Attributes.logging.params.git.repository.namespace"]}
    git.repository.name ${record["Actor.Attributes.logging.params.git.repository.name"]}
    git.commit.id ${record["Actor.Attributes.logging.params.git.commit.id"]}
    container_id ${record["id"]}
    log Event "${record["Action"]}" was reported by container belonging to service "${record["Actor.Attributes.logging.params.service.name"]}". Container id was "${record["id"]}".
    event.name ${record["Action"]}
  </record>
</filter>

<filter docker-container-event.**>
  @type record_transformer
  keep_keys container_id,log,event.name,event.origin,service.name,git.repository.namespace,git.repository.name,git.commit.id
  renew_record
</filter>
