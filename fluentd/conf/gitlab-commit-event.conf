# This configuration file will handle the transformation of "git push" events sent to Gitlab. We transform the event into a common structure that we use in all fluentd configurations. This makes it possible to easily filter by a specific property in Kibana and correlate different types of events.

<filter gitlab-event-listener.commit>
  type flatten_hash
  separator .
</filter>

<filter gitlab-event-listener.commit>
  @type record_transformer
  <record>
    event.origin "gitlab"
    event.name "commit"
    # git.repository.namespace ${record["Actor.Attributes.logging.params.git.repository.namespace"]}
    git.repository.name ${record["payload.repository.name"]}
    git.branch.name ${record["payload.ref"]}

    git.commit.id ${record["payload.commit.id"]}
    git.commit.message ${record["payload.commit.message"]}
    git.commit.author.name ${record["payload.commit.author.name"]}
    git.commit.author.email ${record["payload.commit.author.email"]}

    log Commit ${record["payload.commit.id"]} pushed to Gitlab for project ${record["payload.repository.name"]} with message: ${record["payload.commit.message"]}
  </record>
</filter>

<filter gitlab-event-listener.commit>
  @type record_transformer
  keep_keys event.origin,event.name,git.repository.name,git.branch.name,git.commit.id,git.commit.message,git.commit.author.name,git.commit.author.email,log
  renew_record
</filter>

